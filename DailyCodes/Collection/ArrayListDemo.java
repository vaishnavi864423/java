import java.util.*;
class ArrayListDemo extends ArrayList{
	public static void main(String[]args){

		ArrayListDemo al=new ArrayListDemo();

		//add() method
		al.add(10);
		al.add("Vaishnavi");
		al.add(20f);
		al.add(10);
		al.add(23.23);

		System.out.println(al);

                //isEmpty() method
		System.out.println(al.isEmpty());

                //contains() method
		System.out.println(al.contains("Vaishnavi"));

		//indexOf(Object)
		System.out.println(al.indexOf(10));

		//last IndexOf(Object)
		System.out.println(al.lastIndexOf(10));

		//get(int)
		System.out.println(al.get(2));

		//set(int,E)
		System.out.println(al.set(2,87));
		System.out.println(al);

		//add(int,E)
		al.add(1,"Aish");
		System.out.println(al);

		//addAll(Collection)
		ArrayList al2=new ArrayList();
		al2.add("Rutuja");
		al2.add("Sakshi");
		al2.add("Parthivi");

		al.addAll(al2);
		System.out.println(al);

		//addAll(int,Collection)
		al.addAll(1,al2);
		System.out.println(al);

		//removeRange(int,int)
		al.removeRange(2,3);
		System.out.println(al);

		//object[]toArray()
		Object arr[]=al.toArray();
		System.out.println(arr);
		for(Object data:arr){
			System.out.print(data+" ");
		}
		System.out.println();
		
	

	}
}
