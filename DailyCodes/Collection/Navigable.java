import java.util.*;
class NavigableSetDemo{
	public static void main(String[]args){
		NavigableSet Ns=new TreeSet();

		Ns.add(12);
		Ns.add(12);
		Ns.add(54);
		Ns.add(23);

		System.out.println(Ns);


		//Methods of NavigableSet
		
		//lower(E)
		System.out.println(Ns.lower(12));
		//Floor(E)
		System.out.println(Ns.floor(12));
		//ceiling(E)
		System.out.println(Ns.ceiling(12));
		//higher(E)
		System.out.println(Ns.higher(12));
		//pollFirst()
		System.out.println(Ns.pollFirst());
		//polllast()
		System.out.println(Ns.pollLast());
		//iterator()
		Iterator itr=Ns.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
		//subset()
		System.out.println(Ns.subSet(10,true,21,false));
		//headset()
		System.out.println(Ns.headSet(12));
		//tailset()
		System.out.println(Ns.tailSet(11));

	}
}
