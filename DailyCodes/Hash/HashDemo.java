import java.util.*;
class HashMapDemo{
	public static void main(String[]args){
		HashSet hs=new HashSet();

		hs.add("Rutuja");
		hs.add("Sakshi");
		hs.add("Vaishnavi");
		hs.add("Parthivi");


		System.out.println(hs);

		HashMap hm=new HashMap();
		hm.put("Rutuja","NESP");
		hm.put("Sakshi","VidyaDham");
		hm.put("Vaishnavi","NESP");
		hm.put("Parthivi","Abhinav");
		//If Key repeated it can give updated value of key
		hm.put("Sakshi","Zeal"); 

		System.out.println(hm);
	}
}
