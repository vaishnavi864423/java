import java.util.Scanner;
import java.util.concurrent.*;
class MyThread implements Runnable{
	Scanner sc=new Scanner(System.in);
	int num=sc.nextInt();
	MyThread(int num){
		this.num=num;
	}
	public void run(){
		System.out.println(Thread.currentThread() + "StartThread:" +num);
		dailyTask();

		System.out.println(Thread.currentThread()+"EndThread:"+num);
	}

	void dailyTask(){
		try{
			Thread.sleep(8000);
		}
		catch(InterruptedException ie){
		}
	}
}

class ThreadPoolDemo{
	public static void main(String[]args){
		ExecutorService ser=Executors.newCachedThreadPool();


		for(int i=1;i<=6;i++){
			MyThread obj=new MyThread(i);
			ser.execute(obj);
		}
		ser.shutdown();
	}
}
