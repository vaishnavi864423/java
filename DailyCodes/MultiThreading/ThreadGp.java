class MyThread extends Thread{
	MyThread(ThreadGroup gp,String str){
		super(gp,str);
	}

	public void run(){
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(5000);
		}
		catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}
class ThreadGroupDemo{
	public static void main(String[]args){

		ThreadGroup pThreadGp=new ThreadGroup("Unix");

		MyThread t1=new MyThread(pThreadGp,"Apple");
		t1.start();

		MyThread t2=new MyThread(pThreadGp,"Linux");
		t2.start();

		MyThread t3=new MyThread(pThreadGp,"Windows");
		t3.start();

		System.out.println(pThreadGp.activeCount());
		System.out.println(pThreadGp.activeGroupCount());

	}
}
